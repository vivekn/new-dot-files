package main

import "fmt"

func main() {
    c := make(chan string)
    go func(c chan string) {
        c <- ("Hello")
    }(c)
    fmt.Println(<-c)
}
