
call pathogen#infect()
set tabstop=4
set shiftwidth=4
set smarttab
set expandtab
set softtabstop=4
set autoindent 
set smartindent
autocmd BufRead *.py set smartindent cinwords=if,elif,else,for,while,try,except,finally,def,class 
:map <F9> <Esc>:tabnew<CR>
:map <F2> <Esc>:tabp<CR>
:map <F3> <Esc>:tabn<CR>
:map <F4> :NERDTreeToggle<CR>
:imap <C-e> <C-y>,

filetype on
filetype plugin on
:highlight SpellBad term=underline gui=undercurl guisp=Orange
"Note background set to dark in .vimrc
if has("gui_running")
    highlight Normal guifg=white guibg=black
endif

" Set to auto read when a file is changed from the outside
set autoread

" With a map 'leader' it's possible to do extra key combinations
" like <leader>w saves the current file
let mapleader = ","
let g:mapleader = ","

" Fast saving
nmap <leader>w :w!<cr>

" Fast editing of the .vimrc
map <leader>e :e! ~/.vimrc<cr>


"Paste from system clipboard.
nmap <leader>p "+gP
imap <leader>p "+gP
vnoremap <leader>y "+y
nnoremap <leader>y "+y

" Select all
nmap <leader>s ggVG

" Fast scrolling
set so=7

" When vimrc is edited, reload it
autocmd! bufwritepost vimrc source ~/.vimrc

" => VIM user interface
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"set hlsearch "Highlight search things
set ignorecase
set smartcase
set wildmenu
set incsearch "Make search act like search in modern browsers
set nolazyredraw "Don't redraw while executing macros 

set magic "Set magic on, for regular expressions

set showmatch "Show matching brackets when text indicator is over them
set mat=2 "How many tenths of a second to blink

" No sound on errors
set noerrorbells
set t_vb=
set tm=500

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Colors and Fonts
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
syntax enable "Enable syntax hl
set t_Co=256
if has("gui_running")
  set guioptions-=T
  set guioptions-=r
  set background=dark
  colorscheme mustang
else
  set background=dark
  colorscheme mustang
endif

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Files, backups and undo
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Turn backup off, since most stuff is in SVN, git anyway...
set nobackup
set nowb
set noswapfile

"Persistent undo
try
    if MySys() == "windows"
      set undodir=C:\Windows\Temp
    else
      set undodir=~/.vim_runtime/undodir
    endif

    set undofile
catch
endtry


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Editing mappings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"Move a line of text using ALT+[jk] or Comamnd+[jk] on mac
nmap <M-j> mz:m+<cr>`z
nmap <M-k> mz:m-2<cr>`z
vmap <M-j> :m'>+<cr>`<my`>mzgv`yo`z
vmap <M-k> :m'<-2<cr>`>my`<mzgv`yo`z

"Delete trailing white space, useful for Python ;)
func! DeleteTrailingWS()
  exe "normal mz"
  %s/\s\+$//ge
  exe "normal `z"
endfunc
autocmd BufWrite *.py :call DeleteTrailingWS()

set cmdheight=2

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Surround.vim
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Surround till end of line
nmap <leader>2 v$s

" Surround from beginning of the line"
nmap <leader>1 v^s

"""""""""""Easy Motion"""""""""""
let g:EasyMotion_leader_key = '<leader>m' 

" Settings for VimClojure
let vimclojure#HighlightBuiltins=1      " Highlight Clojure's builtins
let vimclojure#ParenRainbow=1           " Rainbow parentheses'!

" Useful for Clojure.
nmap z va)
" Select current block
let g:slime_target = "screen"

" PyFlakes
let g:pyflakes_autostart = 0
map <F11> :PyflakesToggle<cr>
map <F4> :NERDTreeToggle<cr>
map <D-k> :NERDTreeToggle<cr>
map <D-e> :CtrlP<cr>
set guifont=Monaco:h13
call pathogen#helptags()
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.pdf,*.jar,*.docx,*.xslx,*.dmg,*.png,*.jpg,*.jpeg,*.mp*,*.avi,*/Dropbox/*,*/eclipse/*,*/Videos/*,*/Music/*,*/Library/*,*.plist,Pictures

set ttyfast
set ruler
set visualbell
set encoding=utf-8
set autochdir
set hlsearch
set cm=blowfish
set smartindent
set tabstop=4
set shiftwidth=4
set expandtab
